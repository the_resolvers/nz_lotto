-- New Zealand Powerball
-- PostgreSQL version
-- 

--
-- Test data
--

--
-- Helper functions
--

-- Generates a random integer between 'low' and 'high' (inclusive)
CREATE OR REPLACE FUNCTION random_between(low INT, high INT) 
   RETURNS INT AS
$$
BEGIN
   RETURN floor(random()* (high-low + 1) + low);
END;
$$ language 'plpgsql' STRICT;


-- Generates an array of 'how_many' unique numbers between 'low' and 'high' (inclusive)
-- This function is not quite efficient, but it's never going to be used in any production
--  environment
CREATE OR REPLACE FUNCTION random_set (low INT, high INT, how_many INT) 
   RETURNS INT[] AS
$$
DECLARE
  counter INT := 0;
  line INT[];
  ball INT;
BEGIN
   LOOP
     ball := random_between(low, high);
     IF counter = 0 OR NOT (ball = ANY (line)) THEN
       line := array_append(line, ball);
       counter := counter +1;
       IF counter = how_many THEN
         RETURN line;
       END IF;
     END IF;
   END LOOP;
END;
$$ language 'plpgsql' STRICT;


--
-- Generate some data
--

--
-- Initiate a draw
--
INSERT INTO lotto_draw (id) VALUES (1);

--
-- Generate some tickets (5 by default)
--

-- Change 5 to 2000000 should you wish to see that it still works :)


INSERT INTO lotto_ticket (draw_id) SELECT id FROM lotto_draw, generate_series(1,5);
--INSERT INTO lotto_ticket (draw_id) SELECT id FROM lotto_draw, generate_series(1,2000000);

--
-- Add lines to tickets
--
-- We are adding 16 lines per every ticket for simplicity sake
-- In fact ticket can contain between 4 and 20 lines but 16 is 
-- probably the most popular choice
-- 

-- You might want to disable WAL in PostgreSQL to speed up the following (It's a test data anyways)

--                                                     6 lotto balls         powerball   
INSERT INTO lotto_line (ticket_id, balls) SELECT  id, random_set(1,40,6)||random_between(1,10) FROM lotto_ticket, generate_series(1,16);


--
-- Draw time!!!
--
-- Update the current draw with 6 main balls, one bonus ball and a powerball
-- It's using the same random function here which is not true random in fact.
-- In real life the draw should be performed on some other apparatus that guarantees
-- true randomness

--                                     lotto+bonus=7balls           powerball
UPDATE lotto_draw SET balls = (SELECT  random_set(1,40,7)  ||  random_set(1,10,1)) WHERE id = 1;

-- If you are running your own lotto :)
-- UPDATE lotto_draw SET balls =         {1,2,3,4,5,6, 7,               8};
--

-- Some indices we will need

CREATE INDEX lotto_line_ticket_id ON lotto_line(ticket_id);

-- Analyze the thing

ANALYZE;
 
-- And do not forget to re-enable WAL if you've disabled it

