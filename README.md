# New Zealand Lotto DB scripts #

A bunch of SQL scripts necessary if you want to run a lotto similar to NZ Powerball and do not want to wait for 24 hours for a result. Our scripts do it much faster (well under 30 minutes on a childish machine for 2 million tickets).


### What is this repository for? ###

* This repository is just an example of how one can implement the DB for an application similar to NZ Powerball Lottery

### Usage ###

* The repository contains SQL files adapted to PostgreSQL database. A similar approach can be applied to any other decent RDBMS if required and is currently out of scope.

* Open and _read_ every .sql file before executing it. If you are not sure that it's going to work, do not execute it.

### Environment ###

* For benchmarking purposes the code was executed on an AWS RDS PostgreSQL virtual machine with 2 vCPU cores and 16 GB of RAM.

### Story ###

* The scripts were written and tested while waiting for the results of the $50 million NZ Lotto draw (https://www.rnz.co.nz/news/national/423641/50m-lotto-results-revealed-after-unusually-long-delay)


### Copyright ###

* All of the code is Copyright [The Resolvers Limited](https://www.resolvers.nz/)

### License ###

* [WTFPL](http://www.wtfpl.net)

### Authors ###

* Taras Klish (taras@yond.world)

### Support ###

* None. If this step fails you can try the contact below.
* Taras Klish (taras@yond.world)





### Contributors ###

* Waiting for


