-- New Zealand Powerball
-- PostgreSQL version
-- 

-- Tables

--
-- For simplicity sake we are using PosgreSQL array datatype here.
-- One can try to use Oracle's VARRAYs approach
-- or MySQL JSON datatype to achieve similar results
--

-- Tables with _classic suffix are not used
-- and listed here just as an example.
-- 


--
-- Draw
-- This is what's happennning twice a week
-- 
-- Empty draws need to be created in advance so people can choose 
-- any draw in a future (constrained by business rules)
--

create table lotto_draw (
  id serial not null primary key,
  balls integer[8] -- Normally NULL, updated after the actual draw happens

  -- Not so important fields go below (date, etc.)
 );

-- Classic table (not used)

create table lotto_draw_classic (
  id serial not null primary key,
  ball_1 integer,
  ball_2 integer,
  ball_3 integer,
  ball_4 integer,
  ball_5 integer,
  ball_6 integer,
  ball_bonus integer,
  ball_power integer

  -- Not so important fields go below (date, etc.)
 );

--
-- Tickets and lines
--

-- A ticket is the set of lines.
-- Prices, bundles and deals are ommitted for simplicity
-- but they do not make any difference for a task of determining 
-- of the winners

create table lotto_ticket (
  id serial not null primary key,
  draw_id integer not null references lotto_draw,
  customer_id integer -- deliberately not enforcing NOT NULL 
                      -- Could be an anonymous customer buying a ticket at a retail outlet
  --
  --  Some other not so important attributes like a date of purchase, sales channel, etc.
  );

create table lotto_line (
  id serial not null primary key,
  ticket_id  integer not null references lotto_ticket,
  balls integer[7] not null -- 6 main balls + powerball
  );

-- Classic table (not used)
create table lotto_line_classic (
  id serial not null primary key,
  ticket_id  integer not null references lotto_ticket,
  ball_1     integer not null,
  ball_2     integer not null,
  ball_3     integer not null,
  ball_4     integer not null,
  ball_5     integer not null,
  ball_6     integer not null,
  ball_power integer

  );

create table lotto_result (
  line_id    integer not null, -- Note that we are deliberately not enforcing foreign key
                               --  to avoid check constraint to be fired and the index rebuilt
                               --  on every insert.
                               --  Will need to create it after the draw
  division   text not null
  
  );
