--
-- New Zealand Powerball
-- PostgreSQL version
-- 

--
-- Results Function
--

-- 
-- We will use the function below to find out the intersection
--  of two arrays
--
-- It might be not the most efficient function though
--  as there are many alternative methods of doing that.
--  Additional research is advised if the perfomance is unsatisfactory
--

CREATE OR REPLACE FUNCTION array_intersect(anyarray, anyarray)
  RETURNS anyarray
  language sql
as $FUNCTION$
    SELECT ARRAY(
        SELECT UNNEST($1)
        INTERSECT
        SELECT UNNEST($2)
    );
$FUNCTION$;

