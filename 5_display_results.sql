--
-- New Zealand Powerball
-- PostgreSQL version
-- 

--
-- Display Results
--

-- Summary
SELECT COUNT(l.id) lines, r.division division
  FROM  lotto_result r, lotto_draw d, lotto_ticket t, lotto_line l
  WHERE t.draw_id   = d.id
    AND r.line_id   = l.id
    AND l.ticket_id = t.id
  GROUP BY r.division
  ORDER BY r.division
;


-- All results
SELECT t.id ticket, l.id line, r.division, l.balls line_balls, d.balls draw_balls
  FROM  lotto_result r, lotto_draw d, lotto_ticket t, lotto_line l
  WHERE t.draw_id   = d.id
    AND r.line_id   = l.id
    AND l.ticket_id = t.id
  ORDER BY r.division
;

