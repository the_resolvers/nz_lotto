--
-- New Zealand Powerball
-- PostgreSQL version
-- 

--
-- Results
--

--
-- If you want things to happen faster, disable autovacuum
--


-- Powerball Division 1
-- 6 lotto + 1 powerball match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'PB D1'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND d.balls[1:6] @> l.balls[1:6] -- all 6 match
   AND d.balls[8]   =  l.balls[7]   -- powerball matches
;

-- Powerball Division 2
-- 5 lotto + 1 bonus + 1 powerball match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'PB D2'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND cardinality( array_intersect(d.balls[1:6], l.balls[1:6]) ) = 5 -- 5 lotto balls match
   AND d.balls[8]   =  l.balls[7]   -- powerball matches
   AND d.balls[7:7] <@ l.balls[1:6] -- bonus ball matches
;

-- Powerball Division 3
-- 5 lotto + 1 powerball match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'PB D3'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND cardinality( array_intersect(d.balls[1:6], l.balls[1:6]) ) = 5 -- 5 lotto balls match
   AND d.balls[8]   =  l.balls[7]   -- powerball matches
   AND NOT d.balls[7:7] <@ l.balls[1:6] -- bonus ball does not match
;

-- Powerball Division 4
-- 4 lotto + 1 bonus + 1 powerball match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'PB D4'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND cardinality( array_intersect(d.balls[1:6], l.balls[1:6]) ) = 4 -- 4 lotto balls match
   AND d.balls[8]   =  l.balls[7]   -- powerball matches
   AND d.balls[7:7] <@ l.balls[1:6] -- bonus ball matches
;

-- Powerball Division 5
-- 4 lotto and 1 powerball match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'PB D5'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND cardinality( array_intersect(d.balls[1:6], l.balls[1:6]) ) = 4 -- 4 lotto balls match
   AND d.balls[8]   =  l.balls[7]   -- powerball matches
   AND NOT d.balls[7:7] <@ l.balls[1:6] -- bonus ball does not match
;

-- Powerball Division 6
-- 3 lotto + 1 bonus + 1 powerball match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'PB D6'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND cardinality( array_intersect(d.balls[1:6], l.balls[1:6]) ) = 3 -- 3 lotto balls match
   AND d.balls[8]   =  l.balls[7]   -- powerball matches
   AND d.balls[7:7] <@ l.balls[1:6] -- bonus ball matches
;


-- Powerball Division 7
-- 3 lotto and 1 powerball match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'PB D7'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND cardinality( array_intersect(d.balls[1:6], l.balls[1:6]) ) = 3 -- 3 lotto balls match
   AND d.balls[8]   =  l.balls[7]    -- powerball matches
   AND NOT d.balls[7:7] <@ l.balls[1:6] -- bonus ball does not match
;


-- Lotto (no powerball)

-- Division 1
-- 6 lotto balls match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'D1'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND d.balls[1:6] @> l.balls[1:6] -- all 6 match
;

--  Division 2
-- 5 lotto + 1 bonus match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'D2'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND cardinality( array_intersect(d.balls[1:6], l.balls[1:6]) ) = 5 -- 5 lotto balls match
   AND d.balls[7:7] <@ l.balls[1:6] -- bonus ball matches
;

--  Division 3
-- 5 lotto balls match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'D3'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND cardinality( array_intersect(d.balls[1:6], l.balls[1:6]) ) = 5 -- 5 lotto balls match
   AND NOT d.balls[7:7] <@ l.balls[1:6] -- bonus ball does not match
;

--  Division 4
-- 4 lotto + 1 bonus match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'D4'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND cardinality( array_intersect(d.balls[1:6], l.balls[1:6]) ) = 4 -- 4 lotto balls match
   AND d.balls[7:7] <@ l.balls[1:6] -- bonus ball matches
;

--  Division 5
-- 4 lotto balls match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'D5'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND cardinality( array_intersect(d.balls[1:6], l.balls[1:6]) ) = 4 -- 4 lotto balls match
   AND NOT d.balls[7:7] <@ l.balls[1:6] -- bonus ball does not match
;

--  Division 6
-- 3 lotto + 1 bonus match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'D6'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND cardinality( array_intersect(d.balls[1:6], l.balls[1:6]) ) = 3 -- 3 lotto balls match
   AND d.balls[7:7] <@ l.balls[1:6] -- bonus ball matches
;

--  Division 7
-- 3 lotto balls match
INSERT INTO lotto_result (line_id, division)
SELECT l.id line, 'D7'
 FROM lotto_line l, lotto_ticket t, lotto_draw d
 WHERE t.draw_id    =  d.id
   AND l.ticket_id  =  t.id
   AND cardinality( array_intersect(d.balls[1:6], l.balls[1:6]) ) = 3 -- 3 lotto balls match
   AND NOT d.balls[7:7] <@ l.balls[1:6] -- bonus ball does not match
;


--
-- Re-enable autovaccum if you've disabled it and enjoy your results
--

